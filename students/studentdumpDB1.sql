-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: students
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `marks`
--

DROP TABLE IF EXISTS `marks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `marks` (
  `marks_id` int(11) NOT NULL,
  `subject` varchar(45) DEFAULT NULL,
  `mark` int(11) DEFAULT NULL,
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`marks_id`),
  KEY `studentID_idx` (`student_id`),
  CONSTRAINT `studentID` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marks`
--

LOCK TABLES `marks` WRITE;
/*!40000 ALTER TABLE `marks` DISABLE KEYS */;
INSERT INTO `marks` VALUES (100,'Математика',5,1),(101,'Математика',5,2),(102,'Математика',2,3),(103,'Математика',3,4),(104,'Математика',5,5),(105,'Математика',5,6),(106,'Математика',4,7),(107,'Математика',3,8),(108,'Математика',4,9),(109,'Математика',3,10),(110,'Математика',5,11),(111,'Математика',2,12),(112,'Java',5,1),(113,'Java',2,2),(114,'Java',4,3),(115,'Java',5,4),(116,'Java',4,5),(117,'Java',3,6),(118,'Java',2,7),(119,'Java',2,8),(120,'Java',4,9),(121,'Java',4,10),(122,'Java',5,11),(123,'Java',5,12),(124,'Java',5,1),(125,'Java',2,2),(126,'Java',4,3),(127,'Java',5,4),(128,'Java',4,5),(129,'Java',3,6),(130,'Java',2,7),(131,'Java',2,8),(132,'Java',4,9),(133,'Java',3,10),(134,'Java',5,11),(135,'Java',5,12),(136,'Java',5,1),(137,'Java',2,2),(138,'Java',2,3),(139,'Java',5,4),(140,'Java',4,5),(141,'Java',3,6),(142,'Java',2,7),(143,'Java',5,8),(144,'Java',4,9),(145,'Java',3,10),(146,'Java',5,11),(147,'Java',5,12);
/*!40000 ALTER TABLE `marks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `student` (
  `student_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `group` varchar(45) DEFAULT NULL,
  `sex` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'Останин Кирилл','Б06','М'),(2,'Гаврилов Максим','Б06','М'),(3,'Розанов Леонид','Б06','М'),(4,'Потапова Дарья','Б02','Ж'),(5,'Карпюка Светлана','Б02','Ж'),(6,'Янзинов Федосий','Б02','М'),(7,'Тянников Тимофей','Б06','М'),(8,'Корсакова Зинаида','Б06','Ж'),(9,'Пестов Владислав','Б02','М'),(10,'Кузьминых Руслан','Б06','М'),(11,'Шкиряк Виталий','Б06','М'),(12,'Капшукова Инна','Б02','Ж');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-08 22:44:37
