DELETE t
FROM students.marks AS t
WHERE student_id IN ( SELECT student_id
FROM (SELECT student_id
FROM students.marks
WHERE mark = 2 
GROUP BY student_id
HAVING COUNT(mark) = 3) x);

DELETE FROM students.student WHERE student_id NOT IN (
SELECT student_id FROM students.marks);
