SELECT student.name, GROUP_CONCAT(marks.mark) as marks
FROM students.student, students.marks 
WHERE marks.subject = 'Java' AND marks.student_id = student.student_id
GROUP BY name;