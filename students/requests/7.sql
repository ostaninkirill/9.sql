SELECT 	student.name,
		COUNT(marks.mark) as count
FROM students.marks, students.student
WHERE marks.student_id = student.student_id
GROUP BY student.name;