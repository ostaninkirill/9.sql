SELECT marks.subject
FROM students.marks
WHERE mark = 2
GROUP BY subject
HAVING COUNT(mark) > 2 