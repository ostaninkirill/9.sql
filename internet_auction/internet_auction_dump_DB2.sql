-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: internet_auction
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bids`
--

DROP TABLE IF EXISTS `bids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bids` (
  `bid_id` int(11) NOT NULL AUTO_INCREMENT,
  `bid_date` date DEFAULT NULL,
  `bid_value` double DEFAULT NULL,
  `items_item_id` int(11) NOT NULL,
  `users_user_id` int(11) NOT NULL,
  PRIMARY KEY (`bid_id`),
  KEY `IDuser_idx` (`users_user_id`),
  KEY `ID_item_idx` (`items_item_id`),
  CONSTRAINT `ID_item` FOREIGN KEY (`items_item_id`) REFERENCES `items` (`item_id`),
  CONSTRAINT `ID_user` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bids`
--

LOCK TABLES `bids` WRITE;
/*!40000 ALTER TABLE `bids` DISABLE KEYS */;
INSERT INTO `bids` VALUES (1,'2018-05-18',52600,1,2),(2,'2018-05-20',53600,1,5),(3,'2018-05-21',54600,1,8),(4,'2018-04-10',14521,2,4),(5,'2018-04-11',16521,2,9),(6,'2018-12-10',5600,3,10),(7,'2019-01-04',16350,4,1),(8,'2019-01-05',16750,4,2),(9,'2018-01-06',17150,4,8),(10,'2018-04-21',20751,5,2),(11,'2018-10-01',26000,6,3),(12,'2018-10-12',30000,6,1),(13,'2018-10-15',34000,6,10),(15,'2018-08-11',200000,8,3),(16,'2019-01-08',356000,9,5);
/*!40000 ALTER TABLE `bids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `items` (
  `item_id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `start_price` double DEFAULT NULL,
  `bid_increment` double DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `stop_date` date DEFAULT NULL,
  `by_it_now` binary(1) DEFAULT NULL,
  `users_user_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `userID_idx` (`users_user_id`),
  CONSTRAINT `userID` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'25 рублей 1989 год.','Русский балет. Палладий - 999 - 31,1 грамм',51600,1000,'2018-05-15','2018-05-30',NULL,1),(2,'20 рублей 1993 год','ММД. НЕ МАГНИТНАЯ. Редкость - RRR',12521,2000,'2018-04-10','2018-04-12',NULL,5),(3,'Рубль 1781 год','Екатерина 2 СПБ ИЗ',5400,200,'2018-12-10','2018-12-30',NULL,3),(4,'Александр Север','денарий \"Пакс\" (UNC) штемпельное состояние',15950,400,'2019-01-01','2019-01-07',NULL,3),(5,'Рубль ГОСМОНЕТА 1802 год','СПБ АИ Александр I.Превосходное качество. Штемпельный блеск',19751,1000,'2018-04-20','2018-04-22',NULL,10),(6,'Рубль 1818 год','СПБ ПС. В слабе NGC AU55',22000,4000,'2018-09-03','2018-10-02',NULL,2),(8,'КОПЕЙКА 1714 год','МАЛОФОРМАТНАЯ',190000,10000,'2018-06-01','2018-08-12',NULL,1),(9,'1 рубль 1725 год','СПБ \"Солнечный в латах\"',336000,20000,'2019-01-07','2019-02-14',NULL,9),(10,'5 копеек 1807 год','ЕМ (перепутка, R2) (060)',29500,500,'2018-06-10','2018-07-05',NULL,6);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `full_name` varchar(45) DEFAULT NULL,
  `billing_address` varchar(100) DEFAULT NULL,
  `login` varchar(20) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Останин Кирилл','Ижевск, ул. Ленина 24-130','Kirill123','fwfeweweee'),(2,'Алеев Роман','Москва, ул. Пушкинская 125-220','RomaA','NUJNGRnu45'),(3,'Бобков Михаил','Сарапул, ул. Горького 8-10','MihaBoba','mogreMFQ1'),(4,'Угличинин Марк','Ижевск, ул. Клубная 5-11','ToyMark','1222ge5'),(5,'Ягренев Алексей','Ижевск, ул. Советская 15-77','Alexxx','fwemKKfe'),(6,'Козлаков Федор','Москва, ул. Ленина 12-110','FedyaYa','123qwerty'),(7,'Зворыгин Илья','Воткинск, ул. Кирова 1-6','IlyaZZZ','12345678a'),(8,'Щедрин Артем','Казань, ул. Фатыха Амирхана 1','Artem','ffffffa12345'),(9,'Живков Илья','Казань, ул. Татарстан 80-20','KazanIlya','wfewwwewfwefwef'),(10,'Кутичева Анна','Ижевск, ул. Пушкинская 2','Anna','1212112wfeefwew');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-08 22:46:25
