DELETE b
FROM internet_auction.bids AS b
INNER JOIN internet_auction.items AS i ON b.items_item_id = i.item_id
WHERE i.users_user_id = 6;

DELETE FROM internet_auction.items
WHERE users_user_id = 6;